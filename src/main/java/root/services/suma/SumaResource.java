
package root.services.suma;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/suma")
public class SumaResource {
    
    @GET
    @Path("{n1}/{n2}")
     public String getSuma( @PathParam("n1") Integer n1,
                           @PathParam("n2") Integer n2 ){
    Integer suma = 0;
    
    suma = n1 + n2;
    
    return "El resulado es: " + suma.toString();
    }
}
